# Open Artist Map

Interactive map showing the artists who created artworks and memorials, based on OpenStreetMap and Wikidata.

## Used technologies

- [OpenStreetMap](https://www.openstreetmap.org/about) and its [`artist:wikidata`](https://wiki.openstreetmap.org/wiki/Key:artist:wikidata) tag
- [Wikidata](https://www.wikidata.org/wiki/Wikidata:Introduction), its [SPARQL Query Service](https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service) and its [P170 (creator)](https://www.wikidata.org/wiki/Property:P170) and [P50 (author)](https://www.wikidata.org/wiki/Property:P50) properties
- [Overpass API](https://wiki.openstreetmap.org/wiki/Overpass_API)
- [MapLibre GL JS](https://maplibre.org/projects/maplibre-gl-js/)

This project is based on [OSM-Wikidata Map Framework](https://gitlab.com/openetymologymap/osm-wikidata-map-framework), for more details see its [README](https://gitlab.com/openetymologymap/osm-wikidata-map-framework/-/blob/main/README.md) and [CONTRIBUTING guide](https://gitlab.com/openetymologymap/osm-wikidata-map-framework/-/blob/main/CONTRIBUTING.md).

## Screenshots
Color grouping by source:
[![Color grouping by source screenshot](images/by_source.jpeg)](https://artist.dsantini.it/#2.324,48.864,15,source,overpass_all_wd+wd_direct)

Color grouping by gender:
[![Color grouping by gender screenshot](images/by_gender.jpeg)](https://artist.dsantini.it/#12.471,41.896,14,gender,overpass_all_wd+wd_direct)

Color grouping by country:
[![Color grouping by country screenshot](images/by_country.jpeg)](https://artist.dsantini.it/#-73.961,40.785,13.2,country,overpass_all_wd+wd_direct)

Artist details:
[![Details panel screenshot](images/details.jpeg)](https://artist.dsantini.it/#2.324,48.864,15,source,overpass_all_wd+wd_direct)
